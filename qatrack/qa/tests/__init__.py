from qatrack.qa.tests.test_views import *
from qatrack.qa.tests.test_models import *
from qatrack.qa.tests.test_tags import *
from qatrack.qa.tests.test_utils import *

__test__ = {
    "views": ["test_views"],
    "models": ["test_models"],
    "utils": ["test_utils"],
    "tags": ["test_tags"],
}
